# Pulling Image
```
IMAGEDIR=/work/${USER}/images
mkdir -p $IMAGEDIR
singularity pull --force --dir $IMAGEDIR oras://gitlab-registry.oit.duke.edu/granek-lab/granek-container-images/mmlong2/mmlong-polishing-simage:latest
```
